## Instrucciones
* Clonar el repositorio 
* Ejecutar: `composer update`
* Levantar web server local con: `php -S 0.0.0.0:8000 -t public public/index.php`

## Uso
 * Ejecutar peticion get al endpoint : "/profile/facebook/211010664918356"
 * repuesta json:
 	{
 		"id": "22656110080132099",
 		"name": "Luis Jose Ramirez Mendez",
 		"last_name": "Ramirez Mendez"
 	}

## endpoints:
 * GET /hello/{name}  
 * GET /profile/facebook/{user}